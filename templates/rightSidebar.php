<?php $title = 'BradPenney.ca'; ?>
<?php $description = 'BradPenney.ca'; ?>
<?php include "../../includes/header.php"; ?>
<?php include "../../includes/nav.php"; ?>

<div class="main">
  <section class="module-small">
    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <div class="post">
            <div class="post-header font-alt">
              <h1 class="post-title">Scheduling Da Jobs</h1>
              <div class="post-meta">By&nbsp;<a href="#">Brad</a> | August 23, 2020 | <a href="#">Linux, </a><a href="#">scheduling</a>
              </div>
            </div>
            <div class="post-entry">
              <p>An absolutely essential task for any System Administrator is scheduling tasks to run at off hours. Linux offers several scheduler options.</p>
              <h2>cron - Standard Scheduling</h2>
              <p>cron is a system daemon that will run a task at a specified time (to the minute). This is exceptionally useful to run re-occurring jobs, such as system maintenance or backups. It has been the standard scheduling tool in Linux and UNIX for several decades.</p>
              <p>Each user has their own separate cron schedule which can be accessed via crontab -e when logged in as that user. In server environments, individual users often do not have (m)any scheduled jobs, but the root user (or other power-user, such as the database user) may have hundreds. </p>
              <blockquote>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.</p>
              </blockquote>
              <p>If several languages coalesce, the grammar of the resulting language is more simple and regular than that of the individual languages. The new common language will be more simple and regular than the existing European languages. It will be as simple as Occidental; in fact, it will be Occidental.</p>
              <ul>
                <li>The European languages are members of the same family.</li>
                <li>Their separate existence is a myth.</li>
                <li>For science, music, sport, etc, Europe uses the same vocabulary.</li>
              </ul>
              <p>The European languages are members of the same family. Their separate existence is a myth. For science, music, sport, etc, Europe uses the same vocabulary. The languages only differ in their grammar, their pronunciation and their most common words.</p>
            </div>
          </div>
        </div>
        <div class="col-sm-4 col-md-3 col-md-offset-1 sidebar">
          <?php include '../../includes/categories.php'; ?>
        </div>
      </div>
    </div>
  </section>


  <hr class="divider-d">
<?php include '../../includes/footer.php'; ?>
