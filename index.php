<?php $title = 'BradPenney.ca'; ?>
<?php $description = 'BradPenney.ca - Open Source Enterprise Technology Articles and Videos'; ?>
<?php include './includes/header.php'; ?>
<?php include './includes/nav.php'; ?>

    <div class="main">
      <section class="module">
        <div class="container">
          <div class="row">
            <div class="col-sm-8">
              <div class="row multi-columns-row post-columns">
                <div class="col-md-6 col-lg-6">
                  <div class="post">
                    <div class="post-thumbnail"><a href="articles/linuxCLI/managingStorageRHEL8.php"><img src="assets/images/linuxCLI/managingStorageRHEL8.png" alt="Managing Storage in Red Hat 8"/></a>
                    </div>
                    <div class="post-header font-alt">
                      <h2 class="post-title"><a href="articles/linuxCLI/managingStorageRHEL8.php">Managing Storage in Red Hat 8</a></h2>
                      <div class="post-meta">November 2, 2020</div>
                    </div>
                    <div class="post-entry">
                      <p>This video article covers storage management tools like <kbd>parted</kbd>, making a file system, and mounting it in runtime and as well as permanently.  </p>
                    </div>
                    <div class="post-more"><a class="more-link" href="articles/linuxCLI/managingStorageRHEL8.php">Read more</a></div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-6">
                  <div class="post">
                    <div class="post-thumbnail"><a href="articles/linuxCLI/systemLoggingRHEL8.php"><img src="assets/images/linuxCLI/systemLoggingRHEL8.png" alt="System Logging in Red Hat 8"/></a></div>
                    <div class="post-header font-alt">
                      <h2 class="post-title"><a href="articles/linuxCLI/systemLoggingRHEL8.php">System Logging in Red Hat 8</a></h2>
                      <div class="post-meta">October 14, 2020</div>
                    </div>
                    <div class="post-entry">
                      <p>This quick video article discusses some of the tools and techniques (such as <kbd>journalctl</kbd>) used to log system information in Red Hat 8.</p>
                    </div>
                    <div class="post-more"><a class="more-link" href="articles/linuxCLI/resetRootPassword.php">Read more</a></div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-6">
                  <div class="post">
                    <div class="post-thumbnail"><a href="articles/linuxCLI/resetRootPassword.php"><img src="assets/images/linuxCLI/resetRootPassword.png" alt="Resetting Root Password"/></a></div>
                    <div class="post-header font-alt">
                      <h2 class="post-title"><a href="articles/linuxCLI/resetRootPassword.php">Resetting <kbd>root</kbd> Password</a></h2>
                      <div class="post-meta">September 27, 2020</div>
                    </div>
                    <div class="post-entry">
                      <p>Linux offers a way to reset the <kbd>root</kbd> password without knowing the current password.  Note: physical access to the computer is required (and a little know how!).</p>
                    </div>
                    <div class="post-more"><a class="more-link" href="articles/linuxCLI/resetRootPassword.php">Read more</a></div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-6">
                  <div class="post">
                    <div class="post-thumbnail"><a href="articles/linuxCLI/schedulingJobs.php"><img src="assets/images/linuxCLI/schedulingJobs.png" alt="Scheduling Jobs in Linux"/></a></div>
                    <div class="post-header font-alt">
                      <h2 class="post-title"><a href="articles/linuxCLI/schedulingJobs.php">Scheduling Jobs</a></h2>
                      <div class="post-meta">August 29, 2020</div>
                    </div>
                    <div class="post-entry">
                      <p>An absolutely essential task for any System Administrator is scheduling tasks to run at off hours. Linux offers several scheduler options.</p>
                    </div>
                    <div class="post-more"><a class="more-link" href="articles/linuxCLI/schedulingJobs.php">Read more</a></div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-6">
                  <div class="post">
                    <div class="post-thumbnail"><a href="articles/python/ifStatements.php"><img src="assets/images/python/ifStatements.png" alt="if Statements in Python"/></a></div>
                    <div class="post-header font-alt">
                      <h2 class="post-title"><a href="articles/python/ifStatements.php"><kbd>if</kbd> Statements</a></h2>
                      <div class="post-meta">August 27, 2020</div>
                    </div>
                    <div class="post-entry">
                      <p>Help programs make decisions based on conditions; an if statement determines if something is true, and then performs an action (or skips an action)!</p>
                    </div>
                    <div class="post-more"><a class="more-link" href="articles/python/ifStatements.php">Read more</a></div>
                  </div>
                </div>
                <div class="col-md-6 col-lg-6">
                  <div class="post">
                    <div class="post-thumbnail"><a href="articles/python/managingLists.php"><img src="assets/images/python/managingLists.png" alt="Managing Lists in Python"/></a></div>
                    <div class="post-header font-alt">
                      <h2 class="post-title"><a href="articles/python/managingLists.php">Managing Lists</a></h2>
                      <div class="post-meta">August 16, 2020</div>
                    </div>
                    <div class="post-entry">
                      <p>A fundamental way of arranging data in Python is the list (similar to an array in many other languages). Lists are used to store similar information that belongs together.</p>
                    </div>
                    <div class="post-more"><a class="more-link" href="articles/python/managingLists.php">Read more</a></div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-4 col-md-3 col-md-offset-1 sidebar">
              <div class="widget">
                <h5 class="widget-title font-alt">About Brad <a href="https://gitlab.com/bradpenney" target="_blank"><i class="fa fa-gitlab"></i></a> <a href="https://github.com/bradpenney" target="_blank"><i class="fa fa-github"></i></a> <a href="https://www.linkedin.com/in/bradpenney/" target="_blank"><i class="fa fa-linkedin"></i></a></h5>
                <p>I'm interested in free and open source software (FOSS).  I believe that distributed computing using FOSS is important; we need to ensure that giant privacy-invading, experience-crafting corporations don't control the Internet and how we experience it. It is critical to allow the free transmission and discussion of ideas and knowledge using free (gratis) and open-source software.</p>

                <p>I'm so grateful to live in an age where anyone can learn anything they wish, with virtually no limitations on access to knowledge whatsoever. In addition to computing, I also enjoy making things, from coffee tables to swimming pools and everything in between.</p>

                <p>Using our pocketbooks and page-views to vote, we can shape the Internet and the world to be a better place!</p>

              </div>
            </div>
          </div>
        </div>
      </section>
      <hr class="divider-d">

<?php include './includes/footer.php'; ?>
