<?php $title = 'BradPenney.ca'; ?>
<?php $description = 'BradPenney.ca'; ?>
<?php include './includes/header.php'; ?>
<?php include './includes/nav.php'; ?>

<div class="main">
  <section class="module">
    <div class="container">
      <h1>Python Articles</h1>
      <hr />
      <p>Python is an excellent programming language to learn - its so widely usable that it really becomes a swiss army knife for programmers. Web scraping, data analysis, system scripting, and web hosting are some of the many common uses of Python. In the articles below, it's assumed that (as you're probably running Linux) Python 3 is installed and available. </p>

      <div class="row multi-columns-row post-columns">
        <div class="col-sm-6 col-md-4 col-lg-4">
          <div class="post">
            <div class="post-thumbnail"><a href="articles/python/ifStatements.php"><img src="assets/images/python/ifStatements.png" alt="if Statements in Python"/></a></div>
            <div class="post-header font-alt">
              <h2 class="post-title"><a href="articles/python/ifStatements.php"><kbd>if</kbd> Statements</a></h2>
              <div class="post-meta">August 27, 2020
              </div>
            </div>
            <div class="post-entry">
              <p>Common across nearly all programming languages, the if statement allow a program to make a decision based on inputs (or existing conditions). At its most basic, an if statement determines if something is true, and then performs an action (or skips an action) based on that value.</p>
            </div>
            <div class="post-more"><a class="more-link" href="articles/python/ifStatements.php">Read more</a></div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4">
          <div class="post">
            <div class="post-thumbnail"><a href="articles/python/managingLists.php"><img src="assets/images/python/managingLists.png" alt="Managing Lists in Python"/></a></div>
            <div class="post-header font-alt">
              <h2 class="post-title"><a href="articles/python/managingLists.php">Managing Lists</a></h2>
              <div class="post-meta">August 16, 2020
              </div>
            </div>
            <div class="post-entry">
              <p>A fundamental way of arranging data in Python is the list (similar to an array in many other languages). Lists are used to store similar information that belongs together.</p>
            </div>
            <div class="post-more"><a class="more-link" href="articles/python/managingLists.php">Read more</a></div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4">
          <div class="post">
            <div class="post-thumbnail"><a href="articles/python/workingWithStrings.php"><img src="assets/images/python/workingWithStrings.png" alt="Working with Strings in Python"/></a></div>
            <div class="post-header font-alt">
              <h2 class="post-title"><a href="articles/python/workingWithStrings.php">Working With Strings</a></h2>
              <div class="post-meta">August 13, 2020
              </div>
            </div>
            <div class="post-entry">
              <p>Python is a great language for text manipulation. Lets have a look at some of the ways its possible to work with strings in Python. </p>
            </div>
            <div class="post-more"><a class="more-link" href="articles/python/workingWithStrings.php">Read more</a></div>
          </div>
        </div>
      </div>
          </div>
        </div>
      </section>

      <hr class="divider-d">


<?php include './includes/footer.php'; ?>
