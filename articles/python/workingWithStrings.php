<?php $title = 'Working With Strings in Python - BradPenney.ca'; ?>
<?php $description = 'BradPenney.ca'; ?>
<?php include "../../includes/header.php"; ?>
<?php include "../../includes/nav.php"; ?>

<div class="main">
  <section class="module-small">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="post">
            <div class="post-header font-alt">
              <h1 class="post-title">Working with Strings in Python</h1>
              <div class="post-meta">August 13, 2020</div>
            </div>
            <div class="post-entry">
              <p>Python is a great language for text manipulation. Lets have a look at some of the ways its possible to work with strings in Python. Of course, as Python is a loosely-typed language, declaring a couple string variables in Python is as simple as:</p>

              <pre>
                <span>firstName = "Brad"</span>
                <span>lastName = "Penney"</span>
                <span>print(firstName)</span>
                <span>print(lastName)</span>
              </pre>

              <p>Be careful of quotation marks in Python, as strings can be surrounded either by single quotes (<kbd>' '</kbd>) or double quotes (<kbd>" "</kbd>). If the string itself contains either a single or a double quote, be sure to use the opposite to define it. For example, <kbd>"It's a beautiful day in the neighbourhood"</kbd> would work fine in Python, but <kbd>'It's a beautiful day in the neighbourhood'</kbd> would produce an error.</p>

              <h2>Basic Concatenation</h2>

              <p>In the example declaration above, the two variables would be printed on separate lines. Its easy to put them on the same line:</p>

              <pre>
                <span>>>> print(firstName + lastName)</span>
                <span>BradPenney</span>
              </pre>

              <p>Note that the addition operator simply slams the two strings together. In order to get the strings to print properly with a space between them, it is necessary to actually add the white-space:</p>

              <pre>
                <span>>>> print(firstName + ' ' + lastName)</span>
                <span>Brad Penney</span>
              </pre>

              <p>If you’re wondering why not just have a variable such as <kbd>fullName = "Brad Penney"</kbd>, technically this would work too. However, breaking things into the smallest (reasonable) blocks will make things easier for a multitude of reasons. In general, its easier to build up from small blocks than to break larger blocks apart.</p>

              <h2>Whitespace</h2>

              <p>Similar to other programming languages treatment of strings, the <kbd>\n</kbd> character generates a new line in Python, and the <kbd>\t</kbd> will create a tab. This can be very useful for displaying messages to users or organizing data into easily review-able chunks.</p>

              <h2>Python String Methods</h2>

              <p>Python has thousands of built in methods (behaviours) for string variables. For example, the <kbd>.title()</kbd> method converts a string to “title case” -&gt; meaning the first letter of each word of the string is capitalized. Here’s how this looks:</p>

              <pre>
                <span>>>> basicString = "hello my name is brad"</span>
                <span>>>> print(basicString.title())</span>
                <span>Hello My Name Is Brad</span>
              </pre>

              <p>Notice this method is called without passing a parameter as its implied that whatever variable it was applied to is what is being passed to the variable. Many string methods don’t require any parameters in Python. Also note that the value of the original string doesn’t change when a method is applied, it has to be reassigned to the variable (ie. <kbd>basicString = basicString.title()</kbd>). Depending on requirements, reassignment may or may not be necessary.</p>

              <p>What build-in methods are available for text manipulation? Here are a very few of the most useful methods:</p>
              <table>
                <colgroup>
                <col style="width: 66%" />
                <col style="width: 33%" />
                </colgroup>
                <thead>
                <tr class="header">
                <th>Method</th>
                <th>What the Method Returns</th>
                </tr>
                </thead>
                <tbody>
                <tr class="odd">
                <td><kbd>count()</kbd></td>
                <td>The number of times a specified value occurs in a string</td>
                </tr>
                <tr class="even">
                <td><kbd>endsWith()</kbd></td>
                <td>Boolean value denoting whether the string ends with a specified value</td>
                </tr>
                <tr class="odd">
                <td><kbd>upper()</kbd></td>
                <td>The string in all upper case letters</td>
                </tr>
                <tr class="even">
                <td><kbd>lower()</kbd></td>
                <td>The string in all lower case letters -&gt; <em>very useful in shell scripting</em></td>
                </tr>
                <tr class="odd">
                <td><kbd>strip()</kbd></td>
                <td>The string with any white space before or after removed. Also available are <kbd>rstrip()</kbd> and <kbd>lstrip()</kbd></td>
                </tr>
                </tbody>
              </table>

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <hr class="divider-d">
<?php include '../../includes/footer.php'; ?>
