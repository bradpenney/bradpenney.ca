<?php $title = 'Managing Lists in Python - BradPenney.ca'; ?>
<?php $description = 'BradPenney.ca'; ?>
<?php include "../../includes/header.php"; ?>
<?php include "../../includes/nav.php"; ?>

<div class="main">
  <section class="module-small">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="post">
            <div class="post-header font-alt">
              <h1 class="post-title">Managing Lists in Python</h1>
              <div class="post-meta">August 16, 2020</div>
            </div>
            <div class="post-entry">
              <p>A fundamental way of arranging data in Python is the list (similar to an array in many other languages). Lists are used to store similar information that belongs together. Declaring a list in Python is simple:</p>

              <pre>
                <span>>>> plants = ['chile pie peppers', 'jalapeno peppers', 'habanero peppers', 'carrots', 'red onions']</span>
                <span>>>> print(plants)</span>
                <span>['chile pie peppers', 'jalapeno peppers', 'habanero peppers', 'carrots', 'red onions']</span>
              </pre>

              <p>Accessing a value within the list is predictable, with <kbd>print(plants[2])</kbd> returning habanero peppers as expected. String methods work here too, so <kbd>print(plants[0].title())</kbd> returns <kbd>Chili Pie Peppers</kbd>. A very useful feature is the ability to grab the last item in the list with <kbd>plants[-1]</kbd>, so it isn't necessary to determine the length of the list in order to work with the last item added (normally).</p>

              <h2>Modifying an Item in the List</h2>

              <p>If changing an item is necessary, this is easily accomplished. For the list above, we could change the <kbd>plants[-1]</kbd> value to <em>cucumbers</em> from <em>red onions</em>:</p>

              <pre>
                <span>>>> plants[-1] = 'cucumbers'</span>
                <span>>>> print(plants[-1])</span>
                <span>cucumbers</span>
              </pre>

              <h2>Adding Items to the List</h2>

              <p>Using the <kbd>append()</kbd> method adds items to the list. To add red onions back onto the list above, simply <kbd>plants.append('red onions')</kbd> would add it back as the last item (but <em>cucumbers</em> would remain on the list as well).</p>

              <p>If the goal is to add an item to the list in a specific place, it would be possible to do this via the <kbd>insert()</kbd> method. So, as above, if it would be better if red onions came before cucumbers, we could add it there:</p>

              <pre>
                <span>>>> plants.insert(-1, 'red onions')</span>
                <span>>>> print(plants)</span>
                <span>['chile pie peppers', 'jalapeno peppers', 'habanero peppers', 'carrots', 'red onions', 'cucumbers']</span>
              </pre>

              <p>Obviously, the insert method will work to insert an item at whatever position required, not just near the end of the list.</p>

              <h2>Removing Items from a List</h2>

              <p>If an item was added to a list and should be removed, there is a special statement for that in Python:</p>

              <pre>
                <span>>>> del plants[1]</span>
                <span>>>> print(plants)</span>
                <span>['chile pie peppers', 'habanero peppers', 'carrots', 'red onions', 'cucumbers']</span>
              </pre>

              <p>Notice that jalapeno peppers, (the second item) is now completely gone from the list.</p>

              <p>It is more common to remove an item with a list and then work with it. This can be done with the <kbd>pop()</kbd> method, which without parameter grabs the last item on the list, but can be used to grab any item if supplied as a parameter. Once an item is "popped", it can be worked with as a normal variable:</p>

              <pre>
                <span>>>> rootCrops = plants.pop(2)</span>
                <span>>>> print("My root crops are " + rootCrops + ".")</span>
                <span>My root crops are carrots.</span>
              </pre>

              This may seem similar to simply accessing the list item with <kbd>plants[2],</kbd> however, the difference is that the item has been removed from the list:

              <pre>
                <span>>>> print(plants)</span>
                <span>['chile pie peppers', 'habanero peppers', 'red onions', 'cucumbers']</span>
              </pre>

              <p>In the case where the value of the item is known, but the position is not, it is possible to remove items by value, without reference to the position:</p>

              <pre>
                <span>>>> plants.remove('cucumbers')</span>
                <span>>>> print(plants)</span>
                <span>['chile pie peppers', 'habanero peppers', 'red onions']</span>
              </pre>

              <p>Quick note -> if a value appears twice on the list (certainly possible), the <kbd>remove()</kbd> method will only remove the first value. To remove all values that match the criteria on the list, use a <kbd>while</kbd> loop.</p>

              <h2>Permanently Sorting a List</h2>

              Sorting a list permanently can be done using the <kbd>sort()</kbd> method. This will put the list into alphabetical order - <kbd>plants.sort()</kbd>. In this case, the list is already in alphabetical order, so no change is obvious. Sorting in reverse alphabetical order is also possible, which will produce a change:</p>

              <pre>
                <span>>>> print(plants)</span>
                <span>['chile pie peppers', 'habanero peppers', 'red onions']</span>
                <span>>>> plants.sort(reverse=True)</span>
                <span>>>> print(plants)</span>
                <span>['red onions', 'habanero peppers', 'chile pie peppers']</span>
              </pre>

              <h2>Temporarily Sorting a List</h2>

              <p>Sometimes permanently sorting a list is impractical, or even not allowed, depending on data retention policies. Python provides a way to temporarily sort a list so it can be worked on via the <kbd>sorted()</kbd> method. The temporary nature of the sorting can be seen here:</p>

              <pre>
                <span>>>> print(plants)</span>
                <span>['red onions', 'habanero peppers', 'chile pie peppers']</span>
                <span>>>> print(sorted(plants))</span>
                <span>['chile pie peppers', 'habanero peppers', 'red onions']</span>
                <span>>>> print(plants)</span>
                <span>['red onions', 'habanero peppers', 'chile pie peppers']</span>
              </pre>

              <h2>Reversing a List</h2>

              To reverse a list (ignoring alphabetical sorting), use the <kbd>reverse()</kbd> method, as in <kbd>plants.reverse()</kbd>. Note that this change is permanent, but of course can be used again to revert to the original order.

              <h2>List Length</h2>

              <p>A very useful utility in working with lists is finding their length. This is particularly useful in writing loops, because it tells the loop how many times to repeat itself. The length method is used as follows:</p>

              <pre>
                <span>>>> print(plants)</span>
                <span>['red onions', 'habanero peppers', 'chile pie peppers']</span>
                <span>>>> print(len(plants))</span>
                <span>3</span>
              </pre>

              <h2>Numerical Lists</h2>

              <p>Occasionally, a list of numbers is a useful construct when writing a program. Python has a very quick and easy construct to create a list of numbers:</p>

              <pre>
                <span>>>> numbers = list(range(1,11))</span>
                <span>>>> print(numbers)</span>
                <span>[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]</span>
              </pre>

            </p>There is an even more powerful tool for writing numberical lists in Python, called <em>list comprehensions</em>. Basically, it applies the power of a <kbd>for</kbd> loop to a one-liner to create a list. Below, we create a list of the cubes of all the numbers up to 10:</p>

            <pre>
              <span>>>> cubes = [i**3 for i in range(1,11)]</span>
              <span>>>> print(cubes)</span>
              <span>[1, 8, 27, 64, 125, 216, 343, 512, 729, 1000]</span>
            </pre>

            <h2>Slicing Lists</h2>

            <p>The final concept covered in the brief lesson will be slicing lists. In some circumstances, especially with sorted lists, finding a subset of the numbers is critical. Slicing allows selecting specific items within a list based on their position. For example, if we go back to the list of plants above, we can list only the first 3 plants:</p>

            <pre>
              <span>>>> plants = ['chile pie peppers', 'jalapeno peppers', 'habanero peppers', 'carrots', 'red onions']</span>
              <span>>>> print(plants[0:3])</span>
              <span>['chile pie peppers', 'jalapeno peppers', 'habanero peppers']</span>
            </pre>

            Notice the first three items are selected using the <kbd>[0:3]</kbd>. This could be very useful in many programs - for example, imagine a game where displaying the top three players would be needed. Simply sorting the list and then slicing the top three scores would solve this issue quickly and easily.

            <hr />

            <p>Lists are an important feature of Python. Any program of size will have many lists, and working with them efficiently is a key skill in Python programming.</p>

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <hr class="divider-d">
<?php include '../../includes/footer.php'; ?>
