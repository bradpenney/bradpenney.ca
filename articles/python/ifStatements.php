<?php $title = 'if Statements in Python - BradPenney.ca'; ?>
<?php $description = 'BradPenney.ca'; ?>
<?php include "../../includes/header.php"; ?>
<?php include "../../includes/nav.php"; ?>

<div class="main">
  <section class="module-small">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="post">
            <div class="post-header font-alt">
              <h1 class="post-title"><kbd>if</kbd> Statements in Python</h1>
              <div class="post-meta">August 27, 2020</div>
            </div>
            <div class="post-entry">
              <p>Common across nearly all programming languages, the <kbd>if</kbd> statement allow a program to make a decision based on inputs (or existing conditions). At its most basic, an if statement determines <kbd>if</kbd> something is true, and then performs an action (or skips an action) based on that value.</p>

              <p>Instead of using frivolous examples of <kbd>if</kbd> statements like some tutorials do, the following is a scenario where an <kbd>if</kbd> statement makes a real decision in a useful script. Let's imagine that there is a script which creates a backup of some data. Before it starts, it creates a "lock" file, and when it is done, deletes that "lock" file. The purpose of this is to ensure multiple backups don't run at the same time, thereby creating a backup of the backup (recursive backups = wasted disk space!!). The <kbd>if</kbd> statement example below will check for the existence of the lock file, then only proceed if the lock file doesn't exist.</p>

              <h2>Basic <kbd>if</kbd> Statement</h2>

              <p>Start by creating the lock file and the lock checking script with:</p>

              <pre>
                <span>[brad@manjaro learningPython]$ touch lock</span>
                <span>[brad@manjaro learningPython]$ touch lockCheck.py</span>
                <span>[brad@manjaro learningPython]$ chmod 777 lockCheck.py</span>
              </pre>

              <p>Now the <kbd>lock</kbd> exists, and the <kbd>lockCheck.py</kbd> file exists. Inside <kbd>lockCheck.py</kbd>, add the following:</p>

              <pre>
                <span>#!/usr/bin/python</span>
                <span></span>
                <span># importing Python's built in library to work with files</span>
                <span>import os.path</span>
                <span>from os import path</span>
                <span></span>
                <span># Check to see if the file exists (will give "True" or "False")</span>
                <span>print(path.exists("lock"))</span>
              </pre>

              <p>Once the file runs, it returns:</p>

              <pre>
                <span>[brad@manjaro learningPython]$  ./lockCheck.py</span>
                <span>True</span>
              </pre>

              <p>If the lock file gets deleted, running this script outputs "False":</p>

              <pre>
                <span>[brad@manjaro learningPython]$ ./lockCheck.py</span>
                <span>False</span>
              </pre>

              <p>Now its time to create a simple <kbd>if</kbd> statement in a program to perform an action based on the output of the check. It could look like this:</p>

              <pre>
                <span>#!/usr/bin/python</span>
                <span></span>
                <span># Imports built-in Python library for file systems</span>
                <span>import os.path</span>
                <span>from os import path</span>
                <span></span>
                <span># Check to see if file exists (boolean)</span>
                <span>fileExists = (path.exists("lock"))</span>
                <span></span>
                <span># If file exists, inform user</span>
                <span>if (fileExists):</span>
                <span>print("Lock exists, cannot initiate program")</span>
              </pre>

              <p>If the <kbd>lock</kbd> file is recreated (<kbd>touch lock</kbd>), then running the script results with:</p>

              <pre>
                <span>[brad@manjaro learningPython]$ touch lock</span>
                <span>[brad@manjaro learningPython]$ ./lockCheck.py</span>
                <span>Lock exists, cannot initiate program</span>
              </pre>

              <h2><kbd>if-else</kbd> Statements</h2>

              <p>Being able to evaluate a condition is useful, but making a decision based on that is even better. In the scenario above, what if <kbd>lock</kbd> doesn't exist? Currently, the program just does absolutely nothing:</p>

              <pre>
                <span>[brad@manjaro learningPython]$ rm lock</span>
                <span>[brad@manjaro learningPython]$ ./lockCheck.py</span>
                <span>[brad@manjaro learningPython]$</span>
              </pre>

              <p>A script that does (and outputs) nothing isn't the goal - if the lock doesn't exist, the backup script should run. An <kbd>if-else</kbd> statement will make that happen:</p>

              <pre>
                <span>#!/usr/bin/python</span>
                <span></span>
                <span># Imports built-in Python library for file systems</span>
                <span>import os.path</span>
                <span>from os import path</span>
                <span></span>
                <span># Check to see if file exists (boolean)</span>
                <span>fileExists = (path.exists("lock"))</span>
                <span></span>
                <span># If file exists, inform user</span>
                <span>if (fileExists):</span>
                <span>print("Lock exists, cannot initiate program")</span>
                <span>else:</span>
                <span>print("No lock exists, initiating backup program")</span>
                <span># path to backup program</span>
              </pre>

              <p>Now when the program runs, it determines that the backup lock isn't existing, and proceeds to run the backup program:</p>

              <pre>
                <span>[brad@manjaro learningPython]$ ./lockCheck.py</span>
                <span>No lock exists, initiating backup program</span>
              </pre>

              Python also supports the <kbd>if-elif-else</kbd> paradigm also found in some other languages. The basic syntax is:</p>

              <pre>
                <span>if (condition):</span>
                <span># do this if condition is met</span>
                <span>elif (condition):</span>
                <span># do this if second condition is met</span>
                <span>else:</span>
                <span># do this if no condition is met</span>
              </pre>

              <p>While testing for one or two conditions is acceptable (and common practice), be careful of writing long (clumsy) <kbd>if-elif-else</kbd> statements. Python doesn't have a <kbd>switch</kbd> function built in like many other languages, but they can be created using Dictionary Mapping, see this article -> <a href="https://www.simplifiedpython.net/python-switch-case-statement/" target="_blank">Python Switch Case Statement Tutorial – Three Ways To Implement Python Switch Case Statement</a>.</p>

              <p>Hopefully this quick article has demonstrated a real use for an <kbd>if</kbd> statement in Python!</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <hr class="divider-d">
<?php include '../../includes/footer.php'; ?>
