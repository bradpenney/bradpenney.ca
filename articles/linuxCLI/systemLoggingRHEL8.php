<?php $title = 'System Logging in RHEL 8 - BradPenney.ca'; ?>
<?php $description = 'BradPenney.ca'; ?>
<?php include "../../includes/header.php"; ?>
<?php include "../../includes/nav.php"; ?>

<div class="main">
  <section class="module-small">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="post">
            <div class="post-header font-alt">
              <h1 class="post-title">System Logging in RHEL 8</h1>
              <div class="post-meta">October 14, 2020</div>
            </div>
            <div class="post-entry">
              <p>
                System logging is one of the most important topics for a System Administrator to understand.  This quick video discusses some of the tools and techniques used to log system information in Red Hat 8.
              </p>
              <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/VKxM0MmCw-o" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
              </div>
              <h2>rsyslog</h2>
              <p>
                Since 2007, every version of Red Hat has used the <kbd>rsyslogd</kbd> service to record system activity. The current version, Red Hat 8, is no exception. A simple <kbd>systemctl status rsyslog</kbd> shows that the service is active and running by default.  The file that controls <kbd>rsyslog</kbd> is <kbd>/etc/rsyslog.conf</kbd>. Each rule has a facility that the log is created for, the severity of message to be logged, and a destination of where the log should be stored.  Any extra snap-in files should be located in <kbd>/etc/rsyslog.d/</kbd>. By default, this directory exists but is empty.
              </p>
              <p>
                The default log location for <kbd>rsyslog</kbd> is <kbd>/var/log</kbd>. A quick listing shows that there is extensive default logging, with each log being large and relatively esoteric. Reading logs in this way is usually done via <kbd>grep</kbd> as reading the actual logs is cumbersome and very time consuming.
              </p>
              <h2>journald</h2>
              <p>
                The <kbd>systemd-journald</kbd> service is available and enabled by default on Red Hat 7 and up. This service makes logs much more accessible and messages easier to find. It is the mechanism behind the <kbd>systemctl status</kbd> command, which is one of the most common ways to check the status of any service.  The system journal is written to <kbd>/run/log/journal/&lt;UUID&gt;</kbd>.  The configuration file is found at <kbd>/etc/systemd/journald.conf</kbd>. By default, <kbd>journald</kbd> is automatically cleared on system reboot, but simply changing the storage parameter in <kbd>journald .conf</kbd> to "persistent" will preserve journals across reboots.</p>
              <h2><kbd>journalctl</kbd></h2>
              <p>
                Built into the <kbd>journald</kbd> service is the <kbd>journalctl</kbd> command, which offers a very powerful filtering system so the user can find specific information quickly and easily. For example, it is possible to search using <kbd>journalctl</kbd> to find messages only about a specific service, such as "NetworkManager". Combining multiple filters is even more powerful, so using <kbd>journalctl -p err -u NetworkManager.service</kbd> shows only errors that have been encountered with Network Manager, making it much easier to filter through logs and find the relevant information. <kbd>journalctl</kbd> is a very powerful command, and well worth learning about in detail. <kbd>man journalctl</kbd> is a great place to start.
              </p>
              <h2>logrotate</h2>
              <p>
                Logging information is essential for troubleshooting, but keeping logs forever may not be ideal, particularly if disk space is a consideration. The <kbd>logrotate</kbd> utility is automatically enabled in Red Hat 8, which discards logs after 5 weeks by default.  Logrotate is controlled by <kbd>/etc/logotate.conf</kbd>. Options in this file include rotating logs weekly, how many weeks to keep backlogs, whether to compress the files, and more. Notice it also designates a location for custom logrotate schedules for RPM packages, by default this is found in the <kbd>/etc/logrotate.d/</kbd> directory. Logrotate is run daily via <kbd>anacron</kbd>. This can be easily seen by visiting <kbd>/etc/cron.daily</kbd> and viewing the <kbd>logrotate</kbd> file. As with any task assigned to <kbd>cron.daily</kbd> this will be performed at some point throughout the day when system resources are not too busy.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <hr class="divider-d">
<?php include '../../includes/footer.php'; ?>
