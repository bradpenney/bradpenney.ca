<?php $title = 'Resetting the Root Password - BradPenney.ca'; ?>
<?php $description = 'BradPenney.ca'; ?>
<?php include "../../includes/header.php"; ?>
<?php include "../../includes/nav.php"; ?>

<div class="main">
  <section class="module-small">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="post">
            <div class="post-header font-alt">
              <h1 class="post-title">Resetting the <kbd>root</kbd> Password</h1>
              <div class="post-meta">September 27, 2020</div>
            </div>
            <div class="post-entry">
              <p>
                <strong>*** Disclaimer ***</strong> This is for demonstration and educational purposes only. Enjoy responsibly!
              </p>
              <p>
                Misplacing the <kbd>root</kbd> password on a Linux system can be catastrophic.  Whether intentional (malicious) or accidental, being unable to log into the <kbd>root</kbd> account can bring a system to a standstill - updates, user management, storage management, and a host of other services could be disrupted. Luckily, Linux offers a way to reset the <kbd>root</kbd> password without knowing the current password.  The only caveat - physical access to the computer is required (and a little know how!).
              </p>
              <p>
                This demo will be performed on a CentOS 8 Virtual Machine, but could just as easily be performed on any physical computer (with Linux installed).  The process is basically the same for all Linux computers that use Grub2, with the exception of SELinux usage (see below).  The first step is to reboot the computer and pause at the Grub2 menu (usually a 5 second prompt during boot process to press any key):
              </p>
              <figure>
                <img class="screenshot" src="../../assets/images/linuxCLI/changingRootPassword_01.png" class="screenshot" alt="Grub2 Boot Menu"/>
                <figcaption>Grub2 Boot Menu</figcaption>
              </figure>
              <p>
                The system may have more than one kernel installed, simply select the correct (usually latest) one and press <kbd>e</kbd> in the Grub2 menu to edit the kernel arguments.  This will result in a screen similar to this one:
              </p>

              <figure>
                <img class="screenshot" src="../../assets/images/linuxCLI/changingRootPassword_02.png" alt="Editing the Kernel Arguments"/>
                <figcaption>Editing the Kernel Arguments</figcaption>
              </figure>

              <p>
                In this screen, find the line that starts with <kbd>linux</kbd> and modify the end of it.  The <kbd>rhgb</kbd> argument enables "Red Hat Graphical Boot", while the <kbd>quiet</kbd> argument causes the kernel to not display boot-up messages.  Remove <kbd>rhgb quiet</kbd> and add <kbd>rd.break</kbd>, as seen below. The <kbd>rd.break</kbd> argument causes the boot process to display a <kbd>root</kbd> shell once <kbd>InitRAM</kbd> file system is loaded, before <kbd>systemd</kbd> has been initialized.
              </p>

              <figure>
                <img class="screenshot" src="../../assets/images/linuxCLI/changingRootPassword_03.png" class="screenshot" alt="Add rd.break"/>
                <figcaption>Remove <kbd>rhgb quiet</kbd> and add <kbd>rd.break</kbd></figcaption>
              </figure>

              <p>
                Once the kernel arguments have been modified, press <kbd>Ctrl-X</kbd> to continue.  After booting, a <kbd>root</kbd> shell will appear, without having to enter a password:
              </p>

              <figure>
                <img class="screenshot" src="../../assets/images/linuxCLI/changingRootPassword_04.png" class="screenshot" alt="Root Shell"/>
                <figcaption>Root shell appears without entering a password</kbd></figcaption>
              </figure>

              <p>
                Although a <kbd>root</kbd> shell has a appeared, we've not gained access to the proper system yet.  This prompt points to the <kbd>InitRAM</kbd> file-system, which resides only in RAM during the boot process, not the proper file-system that the system operates in normally.  The normal file-system is actually mounted in <kbd>/sysroot</kbd> directory within the <kbd>InitRAM</kbd> file system.  To make matters worse, <kbd>/sysroot</kbd> is read-only by default (at this stage).  In order to reset the <kbd>root</kbd> password, it is necessary to mount <kbd>/sysroot</kbd> with read/write access.  In the screenshot below, the last line of the result of the <kbd>mount</kbd> command shows current read-only access.  The command to remount the <kbd>/sysroot</kbd> file-system in read/write mode is <kbd>mount -o remount,rw /sysroot</kbd>:
              </p>

              <figure>
                <img class="screenshot" src="../../assets/images/linuxCLI/changingRootPassword_05.png" class="screenshot" alt="Remount the file system"/>
                <figcaption>Unmount and remount the <kbd>/sysroot</kbd> file system in read/write mode</figcaption>
              </figure>

              <p>
                Now that <kbd>/sysroot</kbd> is mounted in read/write mode, the next step is to navigate to the <kbd>/sysroot</kbd> directory and change it to the base directory:
              </p>

              <figure>
                <img class="screenshot" src="../../assets/images/linuxCLI/changingRootPassword_07.png" class="screenshot" alt="Change Root Directory"/>
                <figcaption>Switch into <kbd>/sysroot</kbd> and <kbd>chroot /sysroot</kbd></figcaption>
              </figure>

              <p>
                Notice that once the </kbd>/sysroot</kbd> directory becomes the <kbd>root</kbd> directory, the shell prompt changes.  Once this has been accomplished, it is possible to change the <kbd>root</kbd> password in the standard way:
              </p>

              <figure>
                <img class="screenshot" src="../../assets/images/linuxCLI/changingRootPassword_08.png" class="screenshot" alt="Change Root Password"/>
                <figcaption>Change <kbd>root</kbd> password in standard way</figcaption>
              </figure>

              <p>
                The previously unknown <kbd>root</kbd> password has been changed!  However, as this is a CentOS 8 system with SELinux enabled, we need to create a file to signal SELinux to recreate its permissions.  Failing to do this will result in an un-bootable system, because various <kbd>systemd</kbd> services will not start.  To avoid this, create a file called <kbd>/.autorelabel</kbd>:
              </p>

              <figure>
                <img class="screenshot" src="../../assets/images/linuxCLI/changingRootPassword_09.png" class="screenshot" alt="Create .autolabel"/>
                <figcaption><kbd>touch /.autolabel</kbd></figcaption>
              </figure>

              <p>
                At this point, the procedure is complete.  To summarize, we remounted the main filesystem in read/write, changed the <kbd>root</kbd> password, and prepared SELinux for this change.  Press <kbd>Ctrl+D</kbd> twice, and the system will boot normally, with the new <kbd>root</kbd> password.  This process will take longer than normal, as SELinux needs to recreate its polices (messages from SELinux during boot):
              </p>

              <figure>
                <img class="screenshot" src="../../assets/images/linuxCLI/changingRootPassword_10.png" class="screenshot" alt="SELinux Recrete Policies"/>
                <figcaption>SELinux Recreating its Policies</figcaption>
              </figure>

              <p>
                Mission Complete!  The <kbd>root</kbd> password has been reset on the system without knowing the previous <kbd>root</kbd> password!
              </p>

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <hr class="divider-d">
<?php include '../../includes/footer.php'; ?>
