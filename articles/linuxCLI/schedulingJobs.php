<?php $title = 'Scheduling Jobs - BradPenney.ca'; ?>
<?php $description = 'BradPenney.ca'; ?>
<?php include "../../includes/header.php"; ?>
<?php include "../../includes/nav.php"; ?>

<div class="main">
  <section class="module-small">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="post">
            <div class="post-header font-alt">
              <h1 class="post-title">Scheduling Jobs</h1>
              <div class="post-meta">August 29, 2020</div>
            </div>
            <div class="post-entry">
              <p>An absolutely essential task for any System Administrator is scheduling tasks to run at off hours. Linux offers several scheduler options.</p>

              <h2><kbd>cron</kbd> - Standard Scheduling</h2>

              <p><kbd>cron</kbd> is a system daemon that will run a task at a specified time (to the minute). This is exceptionally useful to run re-occurring jobs, such as system maintenance or backups. It has been the standard scheduling tool in Linux and UNIX for several decades.</p>

              <p>Each user has their own separate <kbd>cron</kbd> schedule which can be accessed via <kbd>crontab -e</kbd> when logged in as that user. In server environments, individual users often do not have (m)any scheduled jobs, but the <kbd>root</kbd> user (or other power-user, such as the database user) may have hundreds. </p>

              <pre>
                <span>* * * * * command(s)</span>
                <span>- - - - -</span>
                <span>| | | | |</span>
                <span>| | | | ----- Day of week (0 - 7) (Sunday=0 or 7)</span>
                <span>| | | ------- Month (1 - 12)</span>
                <span>| | --------- Day of month (1 - 31)</span>
                <span>| ----------- Hour (0 - 23)</span>
                <span>------------- Minute (0 - 59)</span>
              </pre>

              <p>So, the following entry in <kbd>crontab -e</kbd> would run every 30 minutes:</p>

              <pre>
                <span>*/30 * * * * /random/maintenance/script.sh</span>
              </pre>

              <p>Additionally, <kbd>crontab -e</kbd> will accept these values:</p>

              <pre>
                <span>string         meaning</span>
                <span>------         -------</span>
                <span>@reboot        Run once, at startup.</span>
                <span>@yearly        Run once a year, "0 0 1 1 *".</span>
                <span>@annually      (same as @yearly)</span>
                <span>@monthly       Run once a month, "0 0 1 * *".</span>
                <span>@weekly        Run once a week, "0 0 * * 0".</span>
                <span>@daily         Run once a day, "0 0 * * *".</span>
                <span>@midnight      (same as @daily)</span>
                <span>@hourly        Run once an hour, "0 * * * *".</span>
              </pre>

              <a href="https://crontab.guru/" target="_blank">Crontab Guru</a> is a very useful interactive guide to scheduling crontab -e entries. Additionally, the official documentation is available at man 5 crontab, and will fill in many more details.

              <hr />

              <p>There are a couple of "gotchas" with cron:</p>

              <ol>

                <li>
                  <p>There is no STDOUT with cron. Scripts that produce standard output (writes to the terminal) will fail. Instead, redirect the output to a log file (either within crontab -e or within the scrupt itself):</p>

                  <pre>
                    <span># crontab entry with output redirected to a log file</span>
                    <span>*/30 * * * * /random/maintenance/script.sh >> /home/brad/cronLogs/maintenanceScript.log</span>
                  </pre>
                </li>

                <li>
                  </p>Relative paths should not be used in crontab -e. Absolute paths are strongly recommended (even within the shell scripts running). However, this is an easy task to accomplish within any shell script with a $BASEDIR variable:</p>

                  <pre>
                    <span>#!/bin/bash</span>
                    <span></span>
                    <span># Declare the base directory</span>
                    <span>$BASEDIR='/myMainteanceScripts/'</span>
                    <span></span>
                    <span># Run some scripts</span>
                    <span>$BASEDIR/backupDirs.sh</span>
                    <span>$BASEDIR/deleteOldBackups.sh</span>
                    <span>$BASEDIR/cleanTempFiles.sh</span>
                  </pre>

                </li>
              </ol>

              <hr />

              <h2><kbd>anacron</kbd> - Loosely Scheduled</h2>

              <p><kbd>anacron</kbd> is an adaptation of <kbd>cron</kbd>, but allows for tasks to be loosely scheduled based on system activity. When a script is placed within the <kbd>etc/cron.daily</kbd> directory, it will run at some point during the 24 hour cycle, whenever system load is low enough to accommodate it. Other scheduling options include the <kbd>/etc/cron.hourly</kbd>, <kbd>/etc/cron.weekly</kbd> or <kbd>/etc/cron.monthly</kbd> directories.</p>

              <p><kbd>anacron</kbd> is great for servers, but it is also very good for personal devices such as laptops, as scheduled tasks will be run whenever the device is powered on, unlike <kbd>cron</kbd>, where if the time is missed, the task won't run.</p>

              <p>A fairly standard listing of tasks scheduled via <kbd>anacron</kbd> in Ubuntu 20.04 includes:</p>

              <pre>
                <span>cron.hourly:</span>
                <span>total 0</span>
                <span></span>
                <span>cron.monthly:</span>
                <span>total 4</span>
                <span>-rwxr-xr-x 1 root root 313 May 29  2017 0anacron</span>
                <span></span>
                <span>cron.weekly:</span>
                <span>total 16</span>
                <span>-rwxr-xr-x 1 root root 312 May 29  2017 0anacron</span>
                <span>-rwxr-xr-x 1 root root 211 Nov 12  2018 update-notifier-common</span>
                <span>-rwxr-xr-x 1 root root 813 Feb 10  2019 man-db</span>
                <span>-rwxr-xr-x 1 root root 730 Mar  8 12:15 apt-xapian-index</span>
                <span></span>
                <span>cron.daily:</span>
                <span>total 52</span>
                <span>-rwxr-xr-x 1 root root  384 Dec 12  2012 cracklib-runtime</span>
                <span>-rwxr-xr-x 1 root root  311 May 29  2017 0anacron</span>
                <span>-rwxr-xr-x 1 root root  376 Nov 20  2017 apport</span>
                <span>-rwxr-xr-x 1 root root  355 Dec 29  2017 bsdmainutils</span>
                <span>-rwxr-xr-x 1 root root 1478 Apr 20  2018 apt-compat</span>
                <span>-rwxr-xr-x 1 root root  214 Nov 12  2018 update-notifier-common</span>
                <span>-rwxr-xr-x 1 root root  377 Jan 21  2019 logrotate</span>
                <span>-rwxr-xr-x 1 root root 1123 Feb 10  2019 man-db</span>
                <span>-rwxr-xr-x 1 root root 1187 Jul 16  2019 dpkg</span>
                <span>-rwxr-xr-x 1 root root  543 Jul 16  2019 mlocate</span>
                <span>-rwxr-xr-x 1 root root  539 Apr 13 14:19 apache2</span>
              </pre>

              <hr />

              <h2><kbd>at</kbd> - One-Time Tasks</h2>

              <p>System Administrators (and power-users) often need to schedule one-time tasks for later (but don't want the job to re-occur on a schedule). Linux has a very useful tool for this: <kbd>at</kbd>.</p>

              <p>Quick note -> <kbd>at</kbd> isn't installed by default within some Debian distributions. It can be installed with <kbd>apt install at</kbd>. It is installed by default on RHEL-based distributions.</p>

              <p>Before using <kbd>at</kbd>, ensure the atd service is running with <kbd>systemctl status atd</kbd>. If the service isn't active, <kbd>systemctl enable --now atd</kbd> will get the service running in the background. Using at is simple using its basic interactive shell:</p>

              <pre>
                <span>[brad@fedora32 Documents]$ at 4pm</span>
                <span>warning: commands will be executed using /bin/sh</span>
                <span>at> /home/brad/myBackupScript.sh</span>
                <span>at> /home/brad/cleanupDownloads.sh</span>
                <span>at> <EOT></span>
                <span>job 3 at Sat Aug 29 16:00:00 2020</span>
              </pre>

              <p>These commands have set up 2 jobs to run at 4pm, a backup script and a cleanup script. Note that the <EOT> output is caused by <kbd>Ctrl-D</kbd>, which exits the interactive shell. To view scheduled jobs, run <kbd>atq</kbd>:</p>

              <pre>
                <span>[brad@fedora32 Documents]$ atq</span>
                <span>3       Sat Aug 29 16:00:00 2020 a brad</span>
              </pre>

              <p>Finally, to remove a scheduled job, <kbd>atrm</kbd> <jobNumber> will take care of it:</p>

              <pre>
                <span>[brad@fedora32 Documents]$ atrm 3</span>
                <span>[brad@fedora32 Documents]$ atq</span>
                <span>[brad@fedora32 Documents]$</span>
              </pre>

              <hr />

              <h2><kbd>systemd-timers</kbd> - An Updated Way to Schedule Jobs in Linux</h2>

              <p><kbd>systemd timers</kbd> is an updated way to schedule jobs in systems that are running systemd (most of the mainstream distributions now run <kbd>systemd</kbd>). Many new packages (or redeveloped packages) are beginning to utilize it. The documentation includes <kbd>man 7 systemd-timer</kbd> and <kbd>man 7 systemd-time</kbd>. The timer files can be found at <kbd>/usr/lib/systemd/system</kbd>. Listing the timers that are on a system is simple:</p>

              <pre>
                <span>[root@fedora32 ~]# systemctl list-unit-files --type=timer</span>
                <span>UNIT FILE                       STATE     VENDOR PRESET</span>
                <span>chrony-dnssrv@.timer            disabled  disabled</span>
                <span>dnf-makecache.timer             enabled   enabled</span>
                <span>fstrim.timer                    enabled   enabled</span>
                <span>fwupd-refresh.timer             disabled  disabled</span>
                <span>logrotate.timer                 enabled   enabled</span>
                <span>mdadm-last-resort@.timer        static    disabled</span>
                <span>mlocate-updatedb.timer          enabled   enabled</span>
                <span>raid-check.timer                disabled  disabled</span>
                <span>systemd-tmpfiles-clean.timer    static    disabled</span>
                <span>unbound-anchor.timer            enabled   enabled</span>
                <span></span>
                <span>10 unit files listed.</span>
              </pre>

              <p>In order to schedule a job, both a <kbd>.timer</kbd> file and a <kbd>.service</kbd> file are required. The <kbd>.timer</kbd> file contains information about when the job will run, and the <kbd>.service</kbd> file contains information about what will actually run. The advantage is that there are significantly more options and control allocated using this system. For example, in the <kbd>.timer</kbd> file below, the <kbd>OnBootSec</kbd> property determines the job will run 10 minutes after bootup, but then the <kbd>OnUnitInactiveSec</kbd> property determines that the job will also run every hour:</p>

              <pre>
                <span>[root@fedora32 ~]# systemctl cat dnf-makecache.timer</span>
                <span># /usr/lib/systemd/system/dnf-makecache.timer</span>
                <span>[Unit]</span>
                <span>Description=dnf makecache --timer</span>
                <span>ConditionKernelCommandLine=!rd.live.image</span>
                <span># See comment in dnf-makecache.service</span>
                <span>ConditionPathExists=!/run/ostree-booted</span>
                <span>Wants=network-online.target</span>
                <span></span>
                <span>[Timer]</span>
                <span>OnBootSec=10min # Runs 10 minutes after boot</span>
                <span>OnUnitInactiveSec=1h #  Runs every hour thereafter</span>
                <span>Unit=dnf-makecache.service</span>
                <span></span>
                <span>[Install]</span>
                <span>WantedBy=timers.target</span>
              </pre>

              <p>In order to activate the timer, simply use <kbd>systemctl enable <timerName>.timer</kbd> and <kbd>systemctl start <timerName>.timer</kbd> and it will start running on its intended schedule.</p>

              <p>To determine when a job last ran, issue <kbd>systemctl status dnf-makecache.service</kbd>:</p>

              <pre>
                <span>[root@fedora32 ~]# systemctl status dnf-makecache.service</span>
                <span>● dnf-makecache.service - dnf makecache</span>
                <span>Loaded: loaded (/usr/lib/systemd/system/dnf-makecache.service; static; vendor preset: disabled)</span>
                <span>Active: inactive (dead) since Sat 2020-08-29 14:38:30 ADT; 55min ago</span>
                <span>TriggeredBy: ● dnf-makecache.timer</span>
                <span>Main PID: 2963 (code=exited, status=0/SUCCESS)</span>
                <span>CPU: 12.803s</span>
                <span></span>
                <span>Aug 29 14:38:22 fedora32.personalLaptop dnf[2963]: RPM Fusion for Fedora 32 - Free - Updates kB/s | 9.9 kB     00:00</span>
                <span>Aug 29 14:38:23 fedora32.personalLaptop dnf[2963]: RPM Fusion for Fedora 32 - Free - Updates kB/s | 618 kB     00:00</span>
                <span>Aug 29 14:38:24 fedora32.personalLaptop dnf[2963]: RPM Fusion for Fedora 32 - Free kB/s | 3.1 kB     00:00</span>
                <span>Aug 29 14:38:25 fedora32.personalLaptop dnf[2963]: RPM Fusion for Fedora 32 - Nonfree - Updates kB/s | 9.7 kB     00:00</span>
                <span>Aug 29 14:38:25 fedora32.personalLaptop dnf[2963]: RPM Fusion for Fedora 32 - Nonfree - Updates kB/s | 139 kB     00:00</span>
                <span>Aug 29 14:38:26 fedora32.personalLaptop dnf[2963]: RPM Fusion for Fedora 32 - Nonfree    kB/s | 3.1 kB     00:00</span>
                <span>Aug 29 14:38:30 fedora32.personalLaptop dnf[2963]: Metadata cache created.</span>
                <span>Aug 29 14:38:30 fedora32.personalLaptop systemd[1]: dnf-makecache.service: Succeeded.</span>
                <span>Aug 29 14:38:30 fedora32.personalLaptop systemd[1]: Finished dnf makecache.</span>
                <span>Aug 29 14:38:30 fedora32.personalLaptop systemd[1]: dnf-makecache.service: Consumed 12.803s CPU</span>
              </pre>

              <p>Notice that the service ran 55 minutes ago, meaning it will run again in 5 minutes.</p>

              <p>Finally, a quick look at the service file itself shows what is actually executed every hour with this systemd timer:</p>

              <pre>
                <span>[root@fedora32 ~]# systemctl cat dnf-makecache.service</span>
                <span># /usr/lib/systemd/system/dnf-makecache.service</span>
                <span>[Unit]</span>
                <span>Description=dnf makecache</span>
                <span># On systems managed by either rpm-ostree/ostree, dnf is read-only;</span>
                <span># while someone might theoretically want the cache updated, in practice</span>
                <span># anyone who wants that could override this via a file in /etc.</span>
                <span>ConditionPathExists=!/run/ostree-booted</span>
                <span></span>
                <span>After=network-online.target</span>
                <span></span>
                <span>[Service]</span>
                <span>Type=oneshot</span>
                <span>Nice=19</span>
                <span>IOSchedulingClass=2</span>
                <span>IOSchedulingPriority=7</span>
                <span>Environment="ABRT_IGNORE_PYTHON=1"</span>
                <span>ExecStart=/usr/bin/dnf makecache --timer</span>
              </pre>

              <p>Notice that much more control is available, for example the <kbd>Nice=19</kbd> parameter means the job will be given very low priority by the system scheduler, so it will get done but not at the expense of higher priority jobs.</p>

              <hr />

              <p>While <kbd>cron</kbd> is still the most widely used scheduling tool in Linux, <kbd>systemd timers</kbd> are becoming more and more popular, particularly among package developers. All System Administrators and power-users should become familiar with <kbd>cron</kbd>, <kbd>anacron</kbd>, <kbd>at</kbd> and <kbd>systemd-timers</kbd>.</p>
          </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <hr class="divider-d">
<?php include '../../includes/footer.php'; ?>
