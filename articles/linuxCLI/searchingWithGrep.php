<?php $title = 'Searching with grep - BradPenney.ca'; ?>
<?php $description = 'BradPenney.ca'; ?>
<?php include "../../includes/header.php"; ?>
<?php include "../../includes/nav.php"; ?>

<div class="main">
  <section class="module-small">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="post">
            <div class="post-header font-alt">
              <h1 class="post-title">Searching With <kbd>grep</kbd></h1>
              <div class="post-meta">August 3, 2020</div>
            </div>
            <div class="post-entry">
              <p>Taking its name from the historic <kbd>ed</kbd> program, <kbd>grep</kbd> stands for globally search for a regular expression and print matching lines. It is a very powerful tool in Linux, allowing the user to search individual files or entire directories (even entire file systems) for a series of characters (a search pattern) and return those results back to the user. <kbd>grep</kbd> can search using normal characters, but its immense power lies in its use of regular expressions and flags to alter its behaviour.</p>

              <h2><kbd>grep</kbd> - The Basic Query</h2>

              <p>Imagine a file, <kbd>mySongs.csv</kbd>, containing the names of some jazz songs and their respective artists:</p>

              <pre>
                <span>Blue Moon,Magpie Jazz Trio</span>
                <span>Serenity,The Midnight Quartet</span>
                <span>Very Early,Gustav Scheringer Duo</span>
                <span>Dreams of Velvet,Trio of Us</span>
                <span>Feathers,Nova Stella</span>
                <span>Theme for Ernie,Sergio Rossi Trio</span>
              </pre>

              <p>Using <kbd>grep</kbd> to search for all the lines that contained the word "Trio" within that file is straightforward:</p>

              <pre>
                <span># grep <seachTerm> <fileToSearch></span>
                <span>brad@CentOS-8 ~]$ grep Trio mySongs.csv</span>
              </pre>

              <p>This <kbd>grep</kbd> statement would result in:</p>

              <pre>
                <span>brad@CentOS-8 ~]$ grep Trio mySongs.csv</span>
                <span>  Blue Moon,Magpie Jazz Trio</span>
                <span>  Dreams of Velvet,Trio of Us</span>
                <span>  Theme for Ernie,Sergio Rossi Trio</span>
              </pre>

              <h2><kbd>regex</kbd> + <kbd>grep</kbd> - Power Queries</h2>

              <p>The real superpower of <kbd>grep</kbd> is that it can utilize <em>regular expressions</em> (aka <kbd>regex</kbd>) to search. Regular expressions are a sequence of characters that define a search pattern. While <kbd>regex</kbd> is a complicated topic and requires some practice to master, it is possible to start to using <kbd>regex</kbd> to unlock the power of <kbd>grep</kbd> immediately.</p>

              <p>It is important to note that in order to search for a <kbd>regex</kbd> using <kbd>grep</kbd>, the search pattern must be surrounded by single quotes, ie. <kbd>'searchPattern'</kbd>. This allows many special characters, such as the caret, <kbd>^</kbd>, which looks for the search pattern at the start of the line only. </p>

              <p>Here's an example of using the caret:</p>

              <pre>
                <span>  brad@CentOS-8 ~]$ grep '^Trio' mySongs.csv</span>
              </pre>

              <p>In this case, <kbd>regex</kbd> eliminates all matches, because the word "Trio" doesn't appear at the beginning of any lines in the specified file. Conversely, the <kbd>$</kbd> searches for the pattern at the end of the line.</p>

              <p>Doing a search similar to above, but using the <kbd>$</kbd> <kbd>regex</kbd> would look like this:</p>

              <pre>
                <span>brad@CentOS-8 ~]$ grep 'Trio$' mySongs.csv</span>
                <span>  Blue Moon,Magpie Jazz Trio</span>
                <span>  Theme for Ernie,Sergio Rossi Trio</span>
              </pre>

              <p>Now the search pattern is matched by two only lines, unlike the three lines found by the basic search above. "Trio" only appears at the end of two lines, and is buried inside the third. This could eliminate some "false positives" and only return the desired results.</p>

              <p>As mentioned above, Regular Expressions are powerful, and will need to be covered separately. Suffice to say, they're an incredible tool, and used not only with <kbd>grep</kbd>, but with other command line utilities like <kbd>sed</kbd>, <kbd>awk</kbd> and even many programming languages.</p>

              <h2>Useful Flags</h2>

              <p>One of the most useful flags for <kbd>grep</kbd> is <kbd>-i</kbd>, which makes the search case insensitive. This is really useful in all *nix environments, which are (by default) case-sensitive. So, when using the <kbd>-i</kbd> flag, the search result for <kbd>grep -i trio mySongs.csv</kbd> and <kbd>grep -i TRIO mySongs.csv</kbd> would have the same results.</p>

              <p>Another useful feature of <kbd>grep</kbd> is that it can be used to search not only specific files, but entire directories. The <kbd>-r</kbd> flag enables recursive searching, thereby finding any instance of the search pattern in any file within whatever directory is designated. For example:</p>

              <pre>
                <span>brad@CentOS-8 ~]$ grep -r 'Trio$' ~/Documents/grepDemo</span>
                <span>  ./myJazzSongs.csv:Blue Moon,Magpie Jazz Trio</span>
                <span>  ./myJazzSongs.csv:Theme for Ernie,Sergio Rossi Trio</span>
              </pre>

              <p>Notice how the search pattern is found, and the user is informed which file the search pattern appears in. Even more useful, adding the <kbd>-n</kbd> flag also displays the line number where the search pattern appears, so its easy to find (particularly within a log file!). Here's how this looks:</p>

              <pre>
                <span>brad@CentOS-8 ~]$ grep -r 'Trio$' ~/Documents/grepDemo</span>
                <span>  ./myJazzSongs.csv:1:Blue Moon,Magpie Jazz Trio</span>
                <span>  ./myJazzSongs.csv:5:Theme for Ernie,Sergio Rossi Trio</span>
              </pre>

              <p>So the user knows which file and line number to consult regarding their search pattern. In enterprise computing, it is not uncommon to have hundreds of files, each with hundreds of lines, and being able to search through these recursively for a single (<kbd>regex</kbd>) search pattern makes <kbd>grep</kbd> one of the most useful tools available in the Linux world.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <hr class="divider-d">
<?php include '../../includes/footer.php'; ?>
