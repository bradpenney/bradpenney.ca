<?php $title = 'Managing Storage Logging in RHEL 8 - BradPenney.ca'; ?>
<?php $description = 'BradPenney.ca'; ?>
<?php include "../../includes/header.php"; ?>
<?php include "../../includes/nav.php"; ?>

<div class="main">
  <section class="module-small">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="post">
            <div class="post-header font-alt">
              <h1 class="post-title">Managing Storage in RHEL 8</h1>
              <div class="post-meta">November 2, 2020</div>
            </div>
            <div class="post-entry">
              <p>
                Managing storage is crucial to managing servers.  This video covers basic storage techniques in RHEL8, including creating partitions using <kbd>parted</kbd>, making a file system, runtime mounting and permanent mounting.
              </p>
              <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/khPcZ6bVjjU" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
              </div>

              <h2>Disks vs Partitions</h2>
              <p>
                Let's start with a quick review of what a disk is and what a partition is. The disk is the physical device that is connected to the motherboard inside the computer (or via an external port). Partitions are how the disk is divided - one disk can have many partitions but each partition is on a specific disk, at least with basic storage systems.
              </p>
              <p>
                So a server will have one or many physical storage devices connected to it. To list all physical devices, run the <kbd>lsblk</kbd> command. This demo is being performed on a virtual machine, so the disks are <kbd>vda</kbd>, <kbd>vdb</kbd>, and <kbd>vdc</kbd>. This represents 3 hard disks attached to the virtual machine, simulating 3 hard disks installed in one server. On a non-virtual computer, you may encounter <kbd>sda</kbd>, <kbd>hda</kbd> (legacy), or even <kbd>nveme0n1</kbd>, which is a newer type of SSD.
              </p>

              <h2>Master Boot Record vs GUID Partition Table</h2>
              <p>
                The GUID Partition Table is a much more modern and useful system. Master Boot Record, MBR, is a legacy system that should only be used in special cases, but could still be present in older legacy systems. In GPT, up to 128 partitions are available. The default tool to manage partitions in RHEL8 is <kbd>parted</kbd>. Issue the <kbd>parted</kbd> command followed by whatever physical device name you'd like to manage, so in this case it would be <kbd>parted /dev/vdb</kbd>.
              </p>

              <h2>Creating a Partition with <kbd>parted</kbd></h2>
              <p>
                Inside <kbd>parted</kbd>, the <kbd>help</kbd> option lists the available commands and is very helpful. <kbd>print</kbd> shows the current partition table, in this case there is none.
              </p>
              <p>
                So the first step is to designate a label, either MBR or GPT. We'll use GPT, so the command is <kbd>mklabel gpt</kbd>. By the way, if you wanted MBR, the label would be <kbd>mklabel msdos</kbd>, which is counter-intuitive... After you've created the label, the easiest way to create a partition using <kbd>parted</kbd> is to simply type <kbd>mkpart</kbd> and it will prompt you for the required information. Enter the details as requested, and be sure to <kbd>print</kbd> the details before leaving <kbd>parted</kbd> to ensure the intended result has been achieved. The <kbd>quit</kbd> <kbd>parted</kbd>.
              </p>
              <p>
                Once you've created the partition using <kbd>parted</kbd>, issue <kbd>udevadm settle</kbd> to ensure that the kernel has picked up the new storage device.
              </p>
              <h2>Creating a File System on the Partition</h2>
              <p>
                Discussing the various file systems available in RHEL8 is really its own video, suffice to say that XFS is the default file system and we'll stick with that for this demonstration. To ensure you're using the right partition, list them using <kbd>lsblk</kbd>. As mentioned, there are many file systems available, simply typing <kbd>mkfs.+TAB+TAB</kbd> shows a list. In our case, we'll use the default XFS system, so issue <kbd>mkfs.xfs /dev/vdb1</kbd>.
              </p>
              <h2>Runtime Mounting</h2>
              <p>
                So now we have a partition with a valid file system ready to be used by the system. The next step is to create a place that the file system will be mounted - where files saved on the partition will be located within the file system. In our case, we'll create a new file system called <kbd>/data</kbd>, using the <kbd>mkdir /data</kbd> command.
              </p>
              <p>
                Now lets mount this file system in runtime to make sure its actually working correctly. The command will be <kbd>mount /dev/vdb1 /data</kbd>. To double check this, run <kbd>mount</kbd> by itself to find the file system (usually the last entry).
              </p>
              <h2>Permanent Mounting</h2>
              <p>
                As mentioned, this file system is now temporarily mounted - if the system were to reboot, it would no longer be available. To make it permanent, we need to edit <kbd>/etc/fstab</kbd>, where all the file systems are listed. Lets do that now.
              </p>
              <p>
                Use <kbd>vi</kbd> to open <kbd>/etc/fstab</kbd>, and then add a line at the end. First the device name, the location in the file system where it can be found, the file system type, and then defaults, 0 0 will work for this system.
              </p>
              <p>
                After a quick reboot, listing the mounted file systems using the <kbd>mount</kbd> command clearly shows that our partition is permanently mounted and available for usage.
              </p>

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


  <hr class="divider-d">
<?php include '../../includes/footer.php'; ?>
