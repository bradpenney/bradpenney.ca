<?php $title = 'BradPenney.ca'; ?>
<?php $description = 'BradPenney.ca'; ?>
<?php include './includes/header.php'; ?>
<?php include './includes/nav.php'; ?>

<div class="main">
  <section class="module">
    <div class="container">
      <h1>Linux CLI Articles</h1>
      <hr />
      <p>Linux runs the vast majority of the world's enterprise servers, and the standard way to interact with these is the Command Line Interface (CLI).  Nearly all of the world's computing infrastruture relies on command line interface to perform all its tasks, from serving a blog, querying and maintaining banking records, or cutting-edge scientific computing. The Linux CLI is definitely worth learning!</p>
      <div class="row multi-columns-row post-columns">
        <div class="col-sm-6 col-md-4 col-lg-4">
          <div class="post">
            <div class="post-thumbnail"><a href="articles/linuxCLI/managingStorageRHEL8.php"><img src="assets/images/linuxCLI/managingStorageRHEL8.png" alt="Managing Storage in Red Hat 8"/></a>
            </div>
            <div class="post-header font-alt">
              <h2 class="post-title"><a href="articles/linuxCLI/managingStorageRHEL8.php">Managing Storage in Red Hat 8</a></h2>
              <div class="post-meta">November 2, 2020</div>
            </div>
            <div class="post-entry">
              <p>This video article covers storage management tools like <kbd>parted</kbd>, making a file system, and mounting it in runtime and as well as permanently.  </p>
            </div>
            <div class="post-more"><a class="more-link" href="articles/linuxCLI/managingStorageRHEL8.php">Read more</a></div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4">
          <div class="post">
            <div class="post-thumbnail"><a href="articles/linuxCLI/systemLoggingRHEL8.php"><img src="assets/images/linuxCLI/systemLoggingRHEL8.png" alt="System Logging in Red Hat 8"/></a></div>
            <div class="post-header font-alt">
              <h2 class="post-title"><a href="articles/linuxCLI/systemLoggingRHEL8.php">System Logging in Red Hat 8</a></h2>
              <div class="post-meta">October 14, 2020</div>
            </div>
            <div class="post-entry">
              <p>This quick video article discusses some of the tools and techniques (such as <kbd>journalctl</kbd>) used to log system information in Red Hat 8.</p>
            </div>
            <div class="post-more"><a class="more-link" href="articles/linuxCLI/systemLoggingRHEL8.php">Read more</a></div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4">
          <div class="post">
            <div class="post-thumbnail"><a href="articles/linuxCLI/resetRootPassword.php"><img src="assets/images/linuxCLI/resetRootPassword.png" alt="Resetting Root Password"/></a></div>
            <div class="post-header font-alt">
              <h2 class="post-title"><a href="articles/linuxCLI/resetRootPassword.php">Resetting <kbd>root</kbd> Password</a></h2>
              <div class="post-meta">September 27, 2020</div>
            </div>
            <div class="post-entry">
              <p>Linux offers a way to reset the <kbd>root</kbd> password without knowing the current password.  Note: physical access to the computer is required (and a little know how!).</p>
            </div>
            <div class="post-more"><a class="more-link" href="articles/linuxCLI/resetRootPassword.php">Read more</a></div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4">
          <div class="post">
            <div class="post-thumbnail"><a href="articles/linuxCLI/schedulingJobs.php"><img src="assets/images/linuxCLI/schedulingJobs.png" alt="Scheduling Jobs in Linux"/></a></div>
            <div class="post-header font-alt">
              <h2 class="post-title"><a href="articles/linuxCLI/schedulingJobs.php">Scheduling Jobs</a></h2>
              <div class="post-meta">August 29, 2020</div>
            </div>
            <div class="post-entry">
              <p>An absolutely essential task for any System Administrator is scheduling tasks to run at off hours. Linux offers several scheduler options.</p>
            </div>
            <div class="post-more"><a class="more-link" href="articles/linuxCLI/schedulingJobs.php">Read more</a></div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-4">
          <div class="post">
            <div class="post-thumbnail"><a href="articles/linuxCLI/searchingWithGrep.php"><img src="assets/images/linuxCLI/searchingWithGrep.png" alt="Searching With Grep"/></a></div>
            <div class="post-header font-alt">
              <h2 class="post-title"><a href="articles/linuxCLI/searchingWithGrep.php">Searching with <kbd>grep</kbd></a></h2>
              <div class="post-meta">August 3, 2020</div>
            </div>
            <div class="post-entry">
              <p><kbd>grep</kbd> is a very powerful command-line toool, it allows the user to search individual files or entire directories for a search pattern and return matches back to the user.</p>
            </div>
            <div class="post-more"><a class="more-link" href="articles/linuxCLI/searchingWithGrep.php">Read more</a></div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>


      <hr class="divider-d">


<?php include './includes/footer.php'; ?>
