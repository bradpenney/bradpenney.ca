<?php
    //  path should be '/' in production and '/devopsnoob/' in testing
    $siteRoot = '/';
?>

<div class="widget">
  <h5 class="widget-title font-alt">Categories</h5>
  <ul class="icon-list">
    <li><a href="<?= $siteRoot; ?>linuxCLI.php">Linux Command Line - 2</a></li>
    <li><a href="<?= $siteRoot; ?>python.php">Python - 3</a></li>
  </ul>
</div>
