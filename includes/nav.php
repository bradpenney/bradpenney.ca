<?php
    //  path should be '/' in production and '/devopsnoob/' in testing
    $siteRoot = '/';
?>

<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#custom-collapse"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="navbar-brand" href="https://www.bradpenney.ca">BradPenney.ca</a>
    </div>
    <div class="collapse navbar-collapse" id="custom-collapse">
      <ul class="nav navbar-nav navbar-right">
        <!--<li><a href="./about">About</a></li>-->
        <li><a href="<?= $siteRoot; ?>linuxCLI.php">Linux CLI</a></li>
        <li><a href="<?= $siteRoot; ?>python.php">Python</a></li>
      </ul>
    </div>
  </div>
</nav>
