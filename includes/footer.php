<?php
    //  path should be '/' in production and '/devopsnoob/' in testing
    $siteRoot = '/';
?>

    <footer class="footer bg-dark">
      <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <p class="copyright font-alt"><a href="https://www.bradpenney.ca">BradPenney.ca</a></p>
            <p class="font-alt small"><small>Proudly cookie-free and ad-free</small></p>
          </div>
          <div class="col-sm-6">
            <div class="footer-social-links"></i></a><a href="https://gitlab.com/bradpenney" target="_blank"><i class="fa fa-gitlab"></i></a><a href="https://github.com/bradpenney" target="_blank"><i class="fa fa-github"></i></a><a href="https://www.linkedin.com/in/bradpenney/" target="_blank"><i class="fa fa-linkedin"></i></a>
            </div>
          </div>
        </div>
      </div>
    </footer>
    </div>
    <div class="scroll-up"><a href="#totop"><i class="fa fa-angle-double-up"></i></a></div>
    </main>
    <!--
    JavaScripts
    =============================================
    -->
    <script src="<?= $siteRoot; ?>assets/lib/jquery/dist/jquery.js"></script>
    <script src="<?= $siteRoot; ?>assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?= $siteRoot; ?>assets/lib/wow/dist/wow.js"></script>
    <script src="<?= $siteRoot; ?>assets/lib/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.js"></script>
    <script src="<?= $siteRoot; ?>assets/lib/isotope/dist/isotope.pkgd.js"></script>
    <script src="<?= $siteRoot; ?>assets/lib/imagesloaded/imagesloaded.pkgd.js"></script>
    <script src="<?= $siteRoot; ?>assets/lib/flexslider/jquery.flexslider.js"></script>
    <script src="<?= $siteRoot; ?>assets/lib/owl.carousel/dist/owl.carousel.min.js"></script>
    <script src="<?= $siteRoot; ?>assets/lib/smoothscroll.js"></script>
    <script src="<?= $siteRoot; ?>assets/lib/magnific-popup/dist/jquery.magnific-popup.js"></script>
    <script src="<?= $siteRoot; ?>assets/lib/simple-text-rotator/jquery.simple-text-rotator.min.js"></script>
    <script src="<?= $siteRoot; ?>assets/js/plugins.js"></script>
    <script src="<?= $siteRoot; ?>assets/js/main.js"></script>
  </body>
</html>
