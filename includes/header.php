<?php
    //  path should be '/' in production and '/devopsnoob/' in testing
    $siteRoot = '/';
?>

<!DOCTYPE html>
<html lang="en-US" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--
    Document Title
    =============================================
    -->
    <title><?php echo $title; ?></title>
  
    <!-- Default stylesheets -->
    <link href="<?= $siteRoot; ?>assets/lib/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Template specific stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Volkhov:400i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
    <link href="<?= $siteRoot; ?>assets/lib/animate.css/animate.css" rel="stylesheet">
    <link href="<?= $siteRoot; ?>assets/lib/components-font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?= $siteRoot; ?>assets/lib/et-line-font/et-line-font.css" rel="stylesheet">
    <link href="<?= $siteRoot; ?>assets/lib/flexslider/flexslider.css" rel="stylesheet">
    <link href="<?= $siteRoot; ?>assets/lib/owl.carousel/dist/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="<?= $siteRoot; ?>assets/lib/owl.carousel/dist/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="<?= $siteRoot; ?>assets/lib/magnific-popup/dist/magnific-popup.css" rel="stylesheet">
    <link href="<?= $siteRoot; ?>assets/lib/simple-text-rotator/simpletextrotator.css" rel="stylesheet">
    <!-- Main stylesheet and color file -->
    <link href="<?= $siteRoot; ?>assets/css/style.css" rel="stylesheet">
    <link id="color-scheme" href="<?= $siteRoot; ?>assets/css/colors/default.css" rel="stylesheet">

    <!--<link href="<?= $siteRoot; ?>includes/main.css" rel="stylesheet">-->
  </head>

  <body data-spy="scroll" data-target=".onpage-navigation" data-offset="60">
    <main>
      <div class="page-loader">
        <div class="loader">Rendering ...</div>
      </div>
